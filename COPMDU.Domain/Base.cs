﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace COPMDU.Domain
{
    public class Base
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
