﻿using System.Collections.Generic;
using COPMDU.Domain;

namespace COPMDU.Infrastructure.Interface
{
    public interface IResolutionRepository
    {
        IEnumerable<Resolution> Get();
        Resolution GetById(int id);
     }
}