﻿using COPMDU.Domain;
using COPMDU.Domain.Entity;
using COPMDU.Infrastructure.Interface;
using COPMDU.Infrastructure.Util;
using iText.Html2pdf;
using iText.Kernel.Pdf;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace COPMDU.Infrastructure.Repository
{
    public class SignalRepository : ISignalRepository
    {
        private readonly IConfiguration _config;

        private readonly PageSession _client;
        private readonly CopMduDbContext _db;

        private int MinimumPercentageValidation =>
            int.Parse(_config["CopMdu:Signal:MinimumPercentageValidation"]);
        private int MinimumPercentageValueValidation =>
            int.Parse(_config["CopMdu:Signal:MinimumPercentageValueValidation"]);
        private float CmSnrMin =>
            float.Parse(_config["CopMdu:Signal:ValidationRules:CmSnr:Min"]);
        private float CmTxMin =>
            float.Parse(_config["CopMdu:Signal:ValidationRules:CmTx:Min"]);
        private float CmTxMax =>
            float.Parse(_config["CopMdu:Signal:ValidationRules:CmTx:Max"]);
        private float CmtsSnrMin =>
            float.Parse(_config["CopMdu:Signal:ValidationRules:CmtsSnr:Min"]);
        private float CmRxMin =>
            float.Parse(_config["CopMdu:Signal:ValidationRules:CmRx:Min"]);
        private float CmRxMax =>
            float.Parse(_config["CopMdu:Signal:ValidationRules:CmRx:Max"]);

        private bool ValidateSignal =>
            bool.Parse(_config["CopMdu:Signal:ValidateSignal"]);

        private string DefaultAddress =>
            _config["CopMdu:Signal:DefaultAddress"];

        private int ExpireSignal =>
            int.Parse(_config["CopMdu:Signal:TimeExpireSignal"]);

        private string MainUrl => _config["Url:SerenaPooling:Uri"];
        private string Proxy => _config["Url:SerenaPooling:UseProxy"];

        private string MainUrlNew => _config["Url:SerenaPoolingNew:Uri"];
        private string ProxyNew => _config["Url:SerenaPoolingNew:UseProxy"];

        public SignalRepository(IConfiguration config, CopMduDbContext db)
        {
            _config = config;

            if (bool.Parse(_config["CopMdu:EnableNewSignalAPI"]))
            {
                _client = new PageSession(MainUrlNew, config, db);
                if (!string.IsNullOrEmpty(ProxyNew))
                    _client.SetProxy(ProxyNew);
            }
            else
            {
                _client = new PageSession(MainUrl, config, db);
                if (!string.IsNullOrEmpty(Proxy))
                    _client.SetProxy(Proxy);
            }

            _db = db;
        }

        private Boolean ContainDefaultAddress(List<ResultadoPoolingVizinhos.Item> vizinhos)
        {
            var address = DefaultAddress;
            foreach (var item in vizinhos)
            {
                if (item.Endereco.Contains(DefaultAddress))
                    return true;
            }
            return false;
        }

        private async Task<ResultadoPoolingVizinhos> Get(int cid, long contract, string cep)
        {
            if (bool.Parse(_config["CopMdu:EnableNewSignalAPI"]))
            {
                var neighboorsSignal = new List<TerminalData>();
                if(cep == "")
                    neighboorsSignal = await _client.GetObject<List<TerminalData>>(String.Format(_config["Url:SerenaPoolingNew:Path:SignalIe"], contract, cid));
                else
                    neighboorsSignal = await _client.GetObject<List<TerminalData>>(String.Format(_config["Url:SerenaPoolingNew:Path:Signal"], contract, cid, cep));

                var vizinhos = new List<ResultadoPoolingVizinhos.Item>();
                foreach (var item in neighboorsSignal)
                {
                    vizinhos.Add(new ResultadoPoolingVizinhos.Item()
                    {
                        Contrato = (int)item.Contract,
                        MAC = item.SerialAddress,
                        Terminal = item.Terminal,
                        Endereco = item.Address,
                        CableData = TreatCableData(item.CableData)
                    });
                }
                // caso tenha retornado o endereco padrao entende que nao pode fechar esse item
                if (ContainDefaultAddress(vizinhos))
                {
                    vizinhos = new List<ResultadoPoolingVizinhos.Item>();
                    vizinhos.Add(new ResultadoPoolingVizinhos.Item
                    {
                        Contrato = 0,
                        MAC = "",
                        Terminal = "",
                        Endereco = "ENDERECO NAO ENCONTRADO FECHAR URA"
                    });
                }

                return new ResultadoPoolingVizinhos() { Data = DateTime.Now, Contrato = (int)contract, CidContrato = cid, QtdVzinhos = neighboorsSignal.Count, Vizinhos = vizinhos };
            }
            else
            {
                return await _client.GetObject<ResultadoPoolingVizinhos>(String.Format(_config["Url:SerenaPooling:Path:Signal"], cid, contract));
            }
        }

        private IDictionary<string, object> TreatCableData(NetworkData data)
        {
            var networkData = ObjectToDictionaryHelper.ToDictionary(data);

            networkData.Add("STATUS", data.Status);
            networkData.Add("CMTS_SNR", data.CmtsSnr.Value.ToString());
            networkData.Add("TXCABLE", data.TxCable.Value.ToString());
            Add(data.Snr, networkData, "SNR");
            Add(data.Rx, networkData, "RX");

            return networkData;
        }

        private static void Add(List<float> list, IDictionary<string, object> dictionary, string key)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (i == 0)
                    dictionary.Add(key, list[i].ToString());
                else
                    dictionary.Add($"{key}_{i}", list[i].ToString());
            }
        }

        private bool CanRequestSignal(DateTime? expirationDate) =>
            expirationDate == null ||
            DateTime.Now > expirationDate;

        private void DeleteSignal(List<OutageSignal> signals)
        {
            foreach (var signal in signals)
            {
                _db.RemoveRange(signals);
            }
        }

        public async Task<List<OutageSignal>> GetSignal(Outage outage)
        {
            if (CanRequestSignal(outage.SignalExpiration))
            {
                if (outage.Signals != null)
                    DeleteSignal(outage.Signals);

                //TODO tratar retorno de erro do pooling
                var cep = outage.Cep ?? "";
                var pooling = await Get(outage.CityId ?? 0, outage.Contract, cep);
                var treater = new DataTreatment(_db, _config, new DbHelper(_db, _config));
                var signals = treater.TreatList(pooling.Vizinhos);
                var validation = ValidateIfNecessary(signals);
                outage.ValidSignal = validation;
                outage.Signals = signals;
                if (signals.Count > 0)
                {
                    outage.SignalDate = DateTime.Now;
                    outage.SignalExpiration = DateTime.Now.AddMilliseconds(ExpireSignal);
                }

                _db.Update(outage);
                await _db.SaveChangesAsync();

                return signals.OrderBy(s => s.Address).ToList();
            }
            return new List<OutageSignal>();
        }

        private bool ValidateIfNecessary(List<OutageSignal> signals) =>
            !ValidateSignal || Validate(signals);

        private bool ValidateMultipleCableData(List<SignalValue> values, float rangeStart, float? rangeEnd = null)
        {
            if (values.Count == 0)
                return false;

            foreach (var value in values)
                value.Valid = ValidateSingleCableData(value.Value, rangeStart, rangeEnd);
            return values.Count(v => v.Valid) / values.Count * 100 > MinimumPercentageValueValidation;
        }


        /// <summary>
        /// Valida um unico dado de sinal verificando se ele está entre o range
        /// </summary>
        /// <param name="value">Valor de base que será validado</param>
        /// <param name="rangeStart">Intervalo de inicio da validação</param>
        /// <param name="rangeEnd">Intervalo de fim da validação (opcional)</param>
        /// <returns>Verdadeiro se <paramref name="value"/> entre <paramref name="rangeStart"/> e <paramref name="rangeEnd"/></returns>
        private bool ValidateSingleCableData(float value, float rangeStart, float? rangeEnd = null) =>
            value >= rangeStart &&
            value <= (rangeEnd ?? value);

        /// <summary>
        /// Valida o valor parcial sobre o total e verifica se é maior que a porcentagem minima requisitada para ser um valor válido
        /// </summary>
        /// <param name="partial"></param>
        /// <param name="full"></param>
        /// <returns></returns>
        private bool ValidateOver(int partial, int full) =>
            (float)partial / (float)full * 100 > MinimumPercentageValidation;

        public bool Validate(List<OutageSignal> signals)
        {
            var onlineAmount = 0;
            foreach (var signal in signals)
            {
                if (signal.Status?.Name.ToLower().Contains("online") ?? false)
                {
                    onlineAmount++;
                    signal.ValidStatus = true;
                }

                var snrValues = signal.CmSnr.OfType<SignalValue>().ToList();
                var rxValues = signal.CmRx.OfType<SignalValue>().ToList();
                signal.ValidCmSnr = ValidateMultipleCableData(snrValues, CmSnrMin);
                signal.ValidCmTx = ValidateSingleCableData(signal.CmTx, CmTxMin, CmTxMax);
                signal.ValidCmtsSnr = ValidateSingleCableData(signal.CmtsSnr, CmtsSnrMin);
                signal.ValidCmRx = ValidateMultipleCableData(rxValues, CmRxMin, CmRxMax);
            }
            if (signals.Count == 0)
                return false;

            return
                ValidateOver(onlineAmount, signals.Count) &&
                ValidateOver(signals.Count(s => s.ValidCmRx), signals.Count) &&
                ValidateOver(signals.Count(s => s.ValidCmSnr), signals.Count) &&
                ValidateOver(signals.Count(s => s.ValidCmtsSnr), signals.Count) &&
                ValidateOver(signals.Count(s => s.ValidCmTx), signals.Count);
        }
        public string GetDictionaryValueOrDefault(Dictionary<string, string> dictionary, string key) =>
            dictionary.ContainsKey(key) ?
                dictionary[key] :
                "";


        private string AddTd(string value) =>
            $"<td align=\"center\">{value}</td>";

        public string BoolToColor(bool valid) =>
            valid ? "#21B20A" : "#E00B0B";

        public string ColorValidation(float value, bool valid) =>
            $"<span style=\"color:{BoolToColor(valid)}\">{value}</span>";

        public string GetHtml(List<OutageSignal> signals)
        {

            var sb = new StringBuilder();
            sb.Append(@"<table>
                <thead>
                <tr>
                <th> Contrato </th>
                <th> Terminal </th>
                <th> MAC </th>
                <th> Status </th>
                <th> CMTS SNR </th>
                <th> CM TX </th>
                <th> CM RX </th>
                <th> CM SNR </th>
                <th> Endereço </th>
                </tr>
                </thead>
                <tbody>");

            foreach (var signal in signals)
            {
                sb.Append("<tr>");
                sb.Append(AddTd(signal.Contract.ToString()));
                sb.Append(AddTd(signal.Terminal?.Name ?? ""));
                sb.Append(AddTd(signal.Mac));
                sb.Append(AddTd(signal.Status?.Name ?? ""));
                sb.Append(AddTd(ColorValidation(signal.CmtsSnr, signal.ValidCmtsSnr)));
                sb.Append(AddTd(ColorValidation(signal.CmTx, signal.ValidCmTx)));
                sb.Append(AddTds(signal.CmRx.OfType<SignalValue>().ToList()));
                sb.Append(AddTds(signal.CmSnr.OfType<SignalValue>().ToList()));
                sb.Append(AddTd(signal.Address.ToString()));
                sb.Append("</tr>");
            }
            sb.Append("</table>");

            return sb.ToString();
        }

        private string AddTds(List<SignalValue> rxs) =>
            AddTd(String.Join("<br/>",
                rxs.Select(v => ColorValidation(v.Value, v.Valid))
                ));

        public byte[] GetPdf(string htmlContent)
        {
            try
            {
                var workStream = new MemoryStream();


                using (var pdfWriter = new PdfWriter(workStream))
                {
                    pdfWriter.SetCloseStream(false);
                    using (var document = HtmlConverter.ConvertToDocument(htmlContent, pdfWriter))
                    {
                    }
                }

                workStream.Position = 0;
                byte[] retorno = new byte[workStream.Length];
                retorno = workStream.ToArray();

                return retorno;

            }
            catch (Exception ex)
            {
                throw new Exception($"Falha ao tentar gerar o pdf: {ex.Message} \n{ex.StackTrace}", ex);
            }
        }
    }
}
