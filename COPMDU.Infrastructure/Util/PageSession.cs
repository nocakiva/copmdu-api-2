﻿using COPMDU.Domain;
using COPMDU.Infrastructure.ClassAttribute;
using COPMDU.Infrastructure.InterationException;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace COPMDU.Infrastructure.Util
{
    /// <summary>
    /// Gerencia uma sessão com um endereço URL e realiza ações de get e post
    /// </summary>
    public class PageSession : IDisposable
    {
        private readonly HttpClient _client;
        private readonly HttpClientHandler _handler;
        private readonly CopMduDbContext _db;
        private readonly IConfiguration _config;

        private bool LogExternalCommunication =>
            bool.Parse(_config["CopMdu:LogExternalCommunication"]);
    
        public void Log(string path, (string key, string value)[] contents, WebCommLog.MethodType method, int statusCode, string response)
        {
            if (LogExternalCommunication)
            {
                try
                {
                    _db.Add(new WebCommLog
                    {
                        Path = path,
                        Method = method,
                        ResponseCode = statusCode,
                        Uri = _client.BaseAddress.Host,
                        Body = response,
                        Param = ParamToString(contents),
                        Date = DateTime.Now
                    });
                    _db.SaveChanges();

                }
                catch (Exception)
                {
                    //erros de log ingorados
                }
            }
        }

        private string ParamToString((string key, string value)[] contents)
        {
            var param = new StringBuilder();
            if (contents != null)
            {
                foreach (var (key, value) in contents)
                {
                    param.AppendLine($"({key}, {value})");
                }
            }
            return param.ToString();
        }

        public PageSession(string uri, IConfiguration config, CopMduDbContext db, string proxy = null)
        {
            _handler = new HttpClientHandler()
            {
                CookieContainer = new CookieContainer()
            };
            if (proxy != null)
                SetProxy(proxy);
            _client = new HttpClient(_handler)
            {
                BaseAddress = new Uri(uri)
            };
            _client.Timeout = TimeSpan.FromMinutes(2);
            _config = config;
            _db = db;
        }

        /// <summary>
        /// Define o proxy a ser utilizado para as chamadas
        /// </summary>
        /// <param name="proxy"></param>
        public void SetProxy(string proxy) =>
            _handler.Proxy = new WebProxy(proxy);

        /// <summary>
        /// Configura uma credencial para ser utilizada
        /// </summary>
        /// <param name="cache"></param>
        public void SetCredentials(string user, string password)
        {
            var cache = new CredentialCache
            {
                {
                    _client.BaseAddress,
                    "Digest",
                    new NetworkCredential(user, password)
                }
            };
            _handler.Credentials = cache;
        }

        /// <summary>
        /// Valida HttpResponse para ver se existe algum erro
        /// </summary>
        private void ValidateHttpResult(HttpResponseMessage response) 
        {
            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                throw new PageNotFoundException(response.RequestMessage.RequestUri.AbsolutePath);
            }
        }

        /// <summary>
        /// Utiiza o retorno de uma requisição Http e formata a página de web em UTF-8 e retorna como string
        /// </summary>
        private async Task<string> ReadStringUtf8(HttpResponseMessage result)
        {
            var buffer = await result.Content.ReadAsByteArrayAsync();
            var bytes = buffer.ToArray();
            return Encoding.UTF8.GetString(bytes, 0, bytes.Length);
        }

        private FormUrlEncodedContent ConvertToUrlContent(params (string key, string value)[] contents)
        {
            var keyValues = new List<KeyValuePair<string, string>>();
            foreach (var (key, value) in contents)
            {
                keyValues.Add(new KeyValuePair<string, string>(key, value));
            }
            return new FormUrlEncodedContent(keyValues);
        }

        /// <summary>
        /// Executa um metodo post para o endereço e passa os parametros no post
        /// </summary>
        /// <param name="path">Caminho do http que irá executar o metodo post</param>
        /// <param name="contents">Conteúdo que será postado</param>
        /// <returns>Conteudo da página de resposta formatado em UTF8</returns>
        public async Task<string> Post(string path, params (string key, string value)[] contents)
        {
            var result = await _client.PostAsync(path, ConvertToUrlContent(contents));
            var responseBody = await ReadStringUtf8(result);
            Log(path, contents, WebCommLog.MethodType.Post, (int)result.StatusCode, responseBody);
            ValidateHttpResult(result);
            return responseBody;
        }

        /// <summary>
        /// Executa um metodo post para o endereço e passa os parametros no post
        /// </summary>
        /// <param name="path">Caminho do http que irá executar o metodo post</param>
        /// <param name="content">Conteúdo que será postado</param>
        /// <returns>Conteudo da página de resposta formatado em UTF8</returns>
        public async Task<string> Post(string path, HttpContent content)
        {
            var result = await _client.PostAsync(path, content);
            var responseBody = await ReadStringUtf8(result);
            Log(path, null, WebCommLog.MethodType.Post, (int)result.StatusCode, responseBody);
            ValidateHttpResult(result);
            return responseBody;
        }


        /// <summary>
        /// Executa um metodo post para o endereço e passa os parametros no post
        /// </summary>
        /// <param name="path">Caminho do http que irá executar o metodo post</param>
        /// <param name="contents">Conteúdo que será postado</param>
        /// <returns>Enum validado através do attribute <see cref="PagePossibleResultAttribute"/></returns>
        public async Task<T> PostWithValidation<T>(string path, params (string key, string value)[] contents)
        {
            var result = await Post(path, contents);
            return StringTransformation.ValidateResponse<T>(result);
        }

        /// <summary>
        /// Executa um metodo post para o endereço e passa os parametros no post, transforma a resposta de string para um objeto
        /// </summary>
        /// <param name="path">Caminho do http que irá executar o metodo post</param>
        /// <param name="contents">Conteúdo que será postado</param>
        /// <returns>Objeto preenchido com os dados de resposta de acordo com a configuração de suas propriedades <see cref="PageResultProperty"/> </returns>
        public async Task<T> PostConverted<T>(string path, params (string key, string value)[] contents) where T: new()
        {
            var result = await Post(path, contents);
            return StringTransformation.ConvertResponse<T>(result);
        }


        /// <summary>
        /// Executa um metodo get para o endereço e passa os parametros no post, transforma a resposta de string para um objeto
        /// </summary>
        /// <param name="path">Caminho do http que irá executar o metodo post</param>
        /// <param name="contents">Conteúdo que será postado</param>
        /// <returns>Objeto preenchido com os dados de resposta de acordo com a configuração de suas propriedades <see cref="PageResultProperty"/> </returns>
        public async Task<T> GetConverted<T>(string path) where T : new()
        {
            var result = await Get(path);
            return StringTransformation.ConvertResponse<T>(result);
        }


        /// <summary>
        /// Executa um metodo get para o endereço e passa os parametros no post
        /// </summary>
        /// <param name="path">Caminho do http que irá executar o metodo get</param>
        /// <returns>Conteudo da página de resposta formatado em UTF8</returns>
        public async Task<string> Get(string path)
        {
            var token = new CancellationToken();
            var result = await _client.GetAsync(path, token);
            var responseBody = await ReadStringUtf8(result);
            Log(path, null, WebCommLog.MethodType.Get, (int)result.StatusCode, responseBody);

            ValidateHttpResult(result);
            return responseBody;
        }


        public async Task<T> GetObject<T>(string path) =>
            JsonConvert.DeserializeObject<T>(await Get(path));

        /// <summary>
        /// Executa um metodo get para o endereço e passa os parametros no post
        /// </summary>
        /// <param name="path">Caminho do http que irá executar o metodo get</param>
        /// <returns>Enum validado através do attribute <see cref="PagePossibleResultAttribute"/></returns>
        public async Task<T> GetWithValidation<T>(string path)
        {
            var result = await Get(path);
            return StringTransformation.ValidateResponse<T>(result);
        }

        #region Dispose function
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool dispose)
        {
            _client?.Dispose();
        }
        #endregion
    }
}
